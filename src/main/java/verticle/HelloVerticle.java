package verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class HelloVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> future) {
        System.out.println("Welcome to Vertx");
        vertx.createHttpServer()
                .requestHandler(r -> r.response().end("Welcome to Vert.x Intro - yusep maulana")
                ).listen(config().getInteger("http.port", 9090),
                result -> {
                if (result.succeeded()) {
                    future.complete();
                } else {
                    future.fail(result.cause());
                }
            });
    }

    @Override
    public void stop() {
        System.out.println("Shutting down application");
    }
}