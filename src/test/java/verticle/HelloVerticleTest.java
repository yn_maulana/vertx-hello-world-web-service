package verticle;


import io.vertx.core.AbstractVerticle;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.testng.annotations.Test;

public class HelloVerticleTest  extends AbstractVerticle {

    @Test
    public void whenReceivedResponse_thenSuccess(TestContext testContext) {
        Async async = testContext.async();

        vertx.createHttpClient()
                .getNow(9090, "localhost", "/", response -> {
                    response.handler(responseBody -> {
                        testContext.assertTrue(responseBody.toString().contains("Hello"));
                        async.complete();
                    });
                });
    }
}